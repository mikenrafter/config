# Tweaks Module

This module contains xmonad-contrib modules that were lacking a piece of
functionality I desired.  
For example, [`EwmhDesktops.hs`](./EwmhDesktops.hs) needed tweaking to make
[`Modules.Fullscreen`](../Modules/Fullscreen.hs) work properly.

