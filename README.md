# Mikenrafter (aka `v0id`)'s dotfiles

This' a git bare repo.

At some point I'll go through and explain everything in depth, but for now,
RTFC...  

See my [nix](https://gitlab.com/mikenrafter/my-nix-config) config for most
everything I've set up.

See my [conkyrc](.config/conky/nord.conkyrc) config for the basic keybinds on
my variant of XMonad. (scroll down, down, down)

## What is `x`?

1. [~/quotes](quotes) is a list of quotes I've made, and collected.
2. [~/.config/fish/functions/random_quote.fish](~/.config/fish/functions/random_quote.fish)
   parses and chooses a random quote from my quotes file.
3. [~/man](man) are my personal manpages.
4. [~/.wmfc](.wmfc), [~/.wmbc](.wmbc), [~/.wmrc](.wmrc), [~/.faves](.faves) are
   all things used in configuration for my wm... Open them, and they'll make
   sense.
5. [~/wg-default](.wg-default) is the wireguard configuration is
   toggled by the VPN button inside deadd-notification-center.
